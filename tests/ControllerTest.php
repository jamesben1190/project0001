<?php 

require_once("src/Controller.php");
use PHPUnit\Framework\TestCase;
class ControllerTest extends TestCase {

    public $controller;
    public function SetUp():void {
     $this->controller = new Controller();   
    }
   //TC (Login&Logout)

   //TC-1.1
   public function testLoginPageAppear()
    {
        $LoginPageAppear = true;
        $this -> assertTrue($LoginPageAppear,'Login Page appears on Screen');
    }
    //TC-1-2-1
    public function testLoginPageEmpty()
    {
        $username  = $this->controller->getusername();
        $password  = $this->controller->getPassword();
        $input = $username.$password;
        $this->assertEmpty($input,'Empty input');
    }
    //TC-1-2-2
    public function testLoginPageInvalid()
    {
        $this->controller->setusername('Hallo123');
        $this->controller->setPassword('Hallo123@');
        $username  = $this->controller->getusername();
        $password  = $this->controller->getPassword();
        $input = $username.$password;
        $input = false;
        $this -> assertFalse($input,'Invalid login!');
    }   
    //TC-1-2-4
    public function testLoginPageValid()
    {
        $this->controller->setusername('Hallo123');
        $this->controller->setPassword('Hallo123@');
        $username  = $this->controller->getusername();
        $password  = $this->controller->getPassword();
        $input = $username.$password;
        $db = $this->controller->getLoginDetails();
        $loginaccount = $this ->assertEquals($input,$db);

        $loginaccount = true;
        $this -> assertTrue($loginaccount,'Login success, redirected to User Admin Homepage');   
    }
    //TC-1-3
    public function testLogout()
    {
        $LogoutClicked = true;
        $this -> assertTrue($LogoutClicked,'Back to Login Page');
    }

    //TC Create User Profile

    //TC-3-1
    public function testAdminPage()
    {
        $AdminPage = true;
        $this -> assertTrue($AdminPage,'Admin page appears on screen');
    }
    //TC-3-2
    public function testAdminPageAddButton()
    {
        $clickAddProfile = true;
        $this -> assertTrue($clickAddProfile,'Redirecting to Add Profile tab');
    }
    //TC-3-3
    public function testAddProfilePage()
    {
        $this->controller->setName('Manager');
        $Name = $this->controller->getName();
        $this->assertEquals($Name,'Manager');

        $this ->controller->setFunc('Menu','Coupon');
        $Functions = $this -> controller -> getFunc();
        $this->assertEquals($Functions,'Menu','Coupon');

        $this->controller->setDesc('Manager can only access menu and coupon');
        $Description = $this -> controller -> getDesc();
        $this->assertEquals($Description,'Manager can only access menu and coupon');
  
        $Name.$Functions.$Description = true;
        $this -> assertTrue(true,'New profile has been successfully created'); 
    }
    //TC-3-3-1
    public function testAddProfilePageFail()
    {
        $this->controller->setName('');
        $Name = $this->controller->getName();
        $this->assertNotEquals($Name,'Manager');

        $this ->controller->setFunc('');
        $Functions = $this -> controller -> getFunc();
        $this->assertNotEquals($Functions,'Menu','Coupon');

        $this->controller->setDesc('Manager can only access menu and coupon');
        $Description = $this -> controller -> getDesc();
        $this->assertEquals($Description,'Manager can only access menu and coupon');

        $Name.$Functions.$Description = false;
        $this -> assertFalse(false,'Failed to create new profile');     
    }
    //TC Create User Account

    //TC-2-1
    public function testAdminpage2()
    {
        $AdminPage = true;
        $this -> assertTrue($AdminPage,'Admin page appears on screen');
    }
    //TC-2-2
    public function testAdminAddUserbutton()
    {
        $clickAddUser = true;
        $this -> assertTrue($clickAddUser,'Redirected to create user form');
    }
    //TC-2-3
    public function testAdminAddUserInvalid()
    {
        $this->controller->setusername('Hallo123');
        $this->controller->setphonenumber('12345678');

        $username  = $this->controller->getusername();
        $phone     = $this->controller->getphonenumber();
        $input =  $username. $phone; 
        $db = $this->controller->getInvalid();
        $createaccount = $this ->assertNotEquals($input,$db);

        $createaccount = false;
        $this -> assertFalse($createaccount,'Username and Phone already exist. Please enter again'); 
    }
    //TC-2-3-1
    public function testAdminAddUser()
    {
        $this->controller->setusername('Hallo456');
        $this->controller->setphonenumber('277492362');

        $username  = $this->controller->getusername();
        $phone     = $this->controller->getphonenumber();
        $input =  $username. $phone; 
        $db = $this->controller->getFormDetails();
        $createaccount = $this ->assertEquals($input,$db);

        $createaccount = true;
        $this -> assertTrue($createaccount,'New user account has been successfully created.'); 
    }
    //TC-2-4
    public function ShowUserForm()
    {
        //No Test required
    }
}
